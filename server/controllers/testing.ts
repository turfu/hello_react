import express, { Request, Response } from 'express'

import Category from '../models/category'
import Product from '../models/product'
import User from '../models/user'

const testingRouter = express.Router()

testingRouter.post('/reset-db', async (_req: Request, res: Response) => {
    await Category.deleteMany({})
    await Product.deleteMany({})
    await User.deleteMany({})

    res.status(204).end()
})

export default testingRouter
