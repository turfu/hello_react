import React, { useState } from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import { useDispatch } from 'react-redux'

import {
    setNewErrorMessage, setNewSuccessMessage
} from '../reducers/notificationReducer'
import authService from '../services/auth'
import { handleAxiosError } from '../utils'

const PasswordChangeForm: React.FC<{ token: string }> = ({ token }) => {
    const [password1, setPassword1] = useState('')
    const [password2, setPassword2] = useState('')
    const dispatch = useDispatch()

    const changePassword = async (event: any) => {
        event.preventDefault()

        if (password1 !== password2) {
            dispatch(setNewErrorMessage('Passwords do not match'))
            return
        }

        let email
        try {
            const data = await authService.changePassword(token, password1)
            email = data.email
        } catch (err) {
            handleAxiosError(dispatch, err, 'authService.changePassword()')
            return
        }

        dispatch(setNewSuccessMessage(
            `Successfully changed password for account "${email}"`
        ))

        setPassword1('')
        setPassword2('')
    }

    return (
        <Form onSubmit={changePassword}>
            <Form.Group controlId="newPassword1">
                <Form.Label>New password:</Form.Label>
                <Form.Control
                    type="password"
                    id="newPassword"
                    value={password1}
                    onChange={(event) => setPassword1(event.target.value)} />
            </Form.Group>

            <Form.Group controlId="newPassword2">
                <Form.Label>Confirm new password:</Form.Label>
                <Form.Control
                    type="password"
                    id="confirmNewPassword"
                    value={password2}
                    onChange={(event) => setPassword2(event.target.value)} />
            </Form.Group>

            <Button variant="warning" type="submit">
                change password&nbsp;
                <i className="fas fa-unlock" />
            </Button>
        </Form>
    )
}

export default PasswordChangeForm
