import express, { Request, Response } from 'express'

import Product from '../models/product'
import { verifyTokenFromHeader } from '../utils/auth'

const productsRouter = express.Router()

productsRouter.get('/', async (req: Request, res: Response) => {
    let sort
    switch (req.query.order) {
    case 'NAME':
        sort = 'name'
        break
    case 'NAME_DESC':
        sort = '-name'
        break
    case 'PRICE':
        sort = 'price'
        break
    case 'PRICE_DESC':
        sort = '-price'
        break
    default:
        sort = 'name'
        break
    }

    const categoryFilter = req.query.category
        ? { category: req.query.category }
        : {}
    const searchFilter = req.query.search
        ? { $text: { $search: req.query.search } }
        : {}
    const filter = { ...categoryFilter, ...searchFilter }

    let countQuery = Product.find(<any> filter, null)
        .countDocuments()
    let productsQuery = Product.find(<any> filter, null)
        .sort(sort)
        .skip(Number(req.query.skip))
        .limit(Number(req.query.limit))
        .populate('category', { name: 1 })

    if (req.query.minPrice) {
        countQuery = countQuery.where('price')
            .gte(req.query.minPrice)
        productsQuery = productsQuery.where('price')
            .gte(req.query.minPrice)
    }
    if (req.query.maxPrice) {
        countQuery = countQuery.where('price')
            .gte(req.query.minPrice)
        productsQuery = productsQuery.where('price')
            .lte(req.query.maxPrice)
    }

    const count = await countQuery
    const products = await productsQuery
    const cheapest = await Product.find(<any> filter, null)
        .sort('price')
        .limit(1)
    const mostExpensive = await Product.find(<any> filter, null)
        .sort('-price')
        .limit(1)

    res.json({
        count, products,
        cheapest: cheapest.length > 0 ? cheapest[0].price : 0,
        mostExpensive: mostExpensive.length > 0 ? mostExpensive[0].price : 0
    })
})

productsRouter.get('/:id', async (req: Request, res: Response) => {
    const product = await Product.findById(req.params.id)
        .populate('category', { name: 1 })

    if (product)
        res.json(product)
    else
        res.status(404).end()
})

productsRouter.patch('/:id', async (req: Request, res: Response) => {
    const body = req.body
    const decdToken = verifyTokenFromHeader(req)

    if (!decdToken || decdToken.reason !== 'LOGGED_IN') {
        res.status(401).json({ error: 'token missing or invalid' })
        return
    }
    if (decdToken.username !== 'admin') {
        res.status(401).json({ error: 'only admin can update a product' })
        return
    }

    if (body.updateObj)
        await Product.findByIdAndUpdate(req.params.id, body.updateObj)

    res.status(200).end()
})

productsRouter.post('/', async (req: Request, res: Response) => {
    const body = req.body
    const decdToken = verifyTokenFromHeader(req)

    if (!decdToken || decdToken.reason !== 'LOGGED_IN') {
        res.status(401).json({ error: 'token missing or invalid' })
        return
    }
    if (decdToken.username !== 'admin') {
        res.status(401).json({ error: 'only admin can create a product' })
        return
    }

    const product = new Product({
        name: body.name,
        category: body.category,
        description: body.description,
        image: body.image,
        price: body.price
    })
    const savedProduct = await product.save()

    res.status(200).json({ id: savedProduct.id })
})

productsRouter.delete('/:id', async (req: Request, res: Response) => {
    const decdToken = verifyTokenFromHeader(req)

    if (!decdToken || decdToken.reason !== 'LOGGED_IN') {
        res.status(401).json({ error: 'token missing or invalid' })
        return
    }
    if (decdToken.username !== 'admin') {
        res.status(401).json({ error: 'only admin can delete a product' })
        return
    }

    await Product.findByIdAndRemove(req.params.id)
    res.status(204).end()
})

export default productsRouter
