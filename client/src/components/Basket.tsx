import React, { useEffect, useState } from 'react'
import Alert from 'react-bootstrap/Alert'
import Button from 'react-bootstrap/Button'
import Col from 'react-bootstrap/Col'
import Form from 'react-bootstrap/Form'
import Cards, { Focused } from 'react-credit-cards'
import 'react-credit-cards/es/styles-compiled.css'
import Table from 'react-bootstrap/Table'
import { useDispatch } from 'react-redux'

import { triggerAddToBasket } from '../reducers/articleReducer'
import { ProductsCookie } from '../types/cookies'

function Basket() {
    const [cardCvc, setCardCvc] = useState('')
    const [cardExpiry, setCardExpiry] = useState('')
    const [cardFocus, setCardFocus] = useState<Focused>('number')
    const [cardName, setCardName] = useState('')
    const [cardNumber, setCardNumber] = useState('')
    const [products, setProducts] = useState<ProductsCookie>([])
    const [showCardForm, setShowCardForm] = useState(false)
    const [totalPriceEuro, setTotalPriceEuro] = useState(0)
    const [computeTotalPriceTrigger, setComputeTotalPriceTrigger] =
        useState(false)
    const dispatch = useDispatch()

    useEffect(() => {
        let productListStr = window.localStorage.getItem('helloReactProducts')
        let productList: ProductsCookie
        if (productListStr)
            productList = JSON.parse(productListStr)
        else
            productList = []

        let total = 0
        for (const product of productList)
            // XXX, handle other currencies
            total += product.price * product.quantity
        setTotalPriceEuro(total)

        setProducts(productList)
    }, [computeTotalPriceTrigger, setProducts])

    const modifyQuantity = (productId: string, increment: number) => {
        let productListStr = window.localStorage.getItem('helloReactProducts')
        if (!productListStr)
            return

        const productList: ProductsCookie = JSON.parse(productListStr)

        for (let idx = 0; idx < productList.length; ++idx) {
            if (productList[idx].id === productId) {
                productList[idx].quantity += increment
                if (productList[idx].quantity <= 0)
                    productList.splice(idx, 1)

                window.localStorage.setItem('helloReactProducts',
                    JSON.stringify(productList))
                setProducts(productList)

                setComputeTotalPriceTrigger(!computeTotalPriceTrigger)

                dispatch(triggerAddToBasket())
                return
            }
        }
    }

    const removeFromBasket = (productId: string) => {
        let productListStr = window.localStorage.getItem('helloReactProducts')
        if (!productListStr)
            return

        const productList: ProductsCookie = JSON.parse(productListStr)

        for (let idx = 0; idx < productList.length; ++idx)
            if (productList[idx].id === productId) {
                productList.splice(idx, 1)

                window.localStorage.setItem('helloReactProducts',
                    JSON.stringify(productList))
                setProducts(productList)

                setComputeTotalPriceTrigger(!computeTotalPriceTrigger)
                dispatch(triggerAddToBasket())

                return
            }
    }

    const basketTable = () => (
        <Table striped>
            <tbody>
                {products.map(product =>
                    <tr key={product.id}>
                        <td>
                            {product.name}
                        </td>
                        <td>
                            x{product.quantity}
                        </td>
                        <td>
                            <Button variant="secondary" id="basketPlus"
                                onClick={() => modifyQuantity(product.id, 1)}>
                                <i className="fas fa-plus" />
                            </Button>
                            <Button variant="secondary" id="basketMinus"
                                onClick={() => modifyQuantity(product.id, -1)}>
                                <i className="fas fa-minus" />
                            </Button>
                        </td>
                        <td>
                            {product.price * product.quantity}
                            &nbsp; €
                        </td>
                        <td>
                            <Button variant="warning" id="basketRemoveButton"
                                onClick={() => removeFromBasket(product.id)}>
                                remove&nbsp;
                                <i className="fas fa-trash-alt" />
                            </Button>
                        </td>
                    </tr>
                )}
            </tbody>
        </Table>
    )

    const cardForm = () => (
        <>
            <Cards
                cvc={cardCvc}
                expiry={cardExpiry}
                focused={cardFocus}
                name={cardName}
                number={cardNumber}
            />
            <br />

            <Form>
                <Form.Group controlId="cardNumber">
                    <Form.Label>Card number:</Form.Label>
                    <Form.Control
                        type="text"
                        onChange={(event) => { setCardNumber(event.target.value) }}
                        onFocus={() => setCardFocus('number')} />
                </Form.Group>

                <Form.Group controlId="cardName">
                    <Form.Label>Name:</Form.Label>
                    <Form.Control
                        type="text"
                        onChange={(event) => { setCardName(event.target.value) }}
                        onFocus={() => setCardFocus('name')} />
                </Form.Group>

                <Form.Row>
                    <Form.Group as={Col} xs={4} controlId="cardExpiry">
                        <Form.Label>Expiry date:</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="MM/YY"
                            onChange={(event) => { setCardExpiry(event.target.value) }}
                            onFocus={() => setCardFocus('expiry')} />
                    </Form.Group>

                    <Form.Group as={Col} xs={3} controlId="cardCvc">
                        <Form.Label>CVC:</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="123"
                            onChange={(event) => { setCardCvc(event.target.value) }}
                            onFocus={() => setCardFocus('cvc')} />
                    </Form.Group>
                </Form.Row>
            </Form>
        </>
    )

    return (
        <>
            <h1>Basket</h1>

            {basketTable()}

            Total: {totalPriceEuro} € &nbsp; |
            <span style={{ display: showCardForm ? 'none' : '' }}>
                <Button variant="link"
                    onClick={() => {setShowCardForm(true)}}>
                    checkout&nbsp;
                    <i className="far fa-credit-card" />
                </Button>
            </span>
            <br />
            <br />

            <div style={{ display: showCardForm ? '' : 'none' }}>
                <Button variant="secondary"
                    onClick={() => {setShowCardForm(false)}}>
                    cancel checkout
                </Button>

                <Alert variant="info">
                    Info: The card checkout feature is not yet implemented.
                </Alert>

                {cardForm()}
            </div>
        </>
    )
}

export default Basket
