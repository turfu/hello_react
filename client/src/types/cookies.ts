export type ProductsCookie = Array<{
    name: string,
    price: number,
    id: string,
    quantity: number
}>

export type UserCookie = {
    token: string,
    email: string,
    username: string
}
