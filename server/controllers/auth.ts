import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import express, { Request, Response } from 'express'
import nodemailer from 'nodemailer'

import User from '../models/user'
import { verifyTokenFromHeader } from '../utils/auth'

const authRouter = express.Router()

authRouter.post('/login', async (req: Request, res: Response) => {
    const body = req.body

    const user = await User.findOne({ email: body.email })

    const passwordCorrect = user === null
        ? false
        : await bcrypt.compare(body.password, user.passwordHash!)

    if (!(user && passwordCorrect)) {
        res.status(401).json({
            error: 'invalid email or password'
        })
        return
    }

    const userObj = {
        email: user.email,
        username: user.username,
        id: user._id,
        reason: 'LOGGED_IN'
    }
    const token = jwt.sign(userObj, process.env.JWT_SECRET!)

    res.status(200)
        .send({ token, email: user.email, username: user.username })
})

function sendVerificationEmail(
    response: Response, to: string, subject: string, text: string
) {
    const transporter = nodemailer.createTransport({
        host: process.env.EMAIL_VERIFIER_SMTP_URL!,
        port: 465,
        secure: true,
        auth: {
            user: process.env.EMAIL_VERIFIER_SMTP_USERNAME!,
            pass: process.env.EMAIL_VERIFIER_SMTP_PASSWORD!
        }
    })

    // eslint-disable-next-line no-unused-vars
    transporter.verify(function(error, _success) {
        if (error) {
            console.log(error)
            response.status(401).json({ error })
        }
    })

    const mailData = {
        from: process.env.EMAIL_VERIFIER_SMTP_USERNAME!,
        to,
        subject,
        text
    }

    // eslint-disable-next-line no-unused-vars
    transporter.sendMail(mailData, (error, _info) => {
        if (error) {
            console.log(error)
            response.status(401).json({ error })
            return
        }
    })
}

authRouter.post('/new-user', (req: Request, res: Response) => {
    const body = req.body

    const pwHash = bcrypt.hashSync(body.password, 10/* saltRounds */)

    const emailObj = {
        email: body.email,
        username: body.username,
        passwordHash: pwHash,
        reason: 'NEW_USER'
    }
    const emailToken = jwt.sign(emailObj, process.env.JWT_SECRET!)

    if (process.env.NODE_ENV === 'test') {
        res.status(200).json({ token: emailToken })
        return
    }

    sendVerificationEmail(
        res, body.email, 'Account validation',
        `Your account for user "${body.username}" needs to be validated.
Visit the following URL to validate your account: ${process.env.WEBSITE_URL}/validate-account?token=${emailToken}.`)

    res.status(204).end()
})

authRouter.post('/validate-email', async (req: Request, res: Response) => {
    const body = req.body

    const decdToken: any = jwt.verify(body.emailedToken, process.env.JWT_SECRET!)
    if (!decdToken || !decdToken.email
        || !decdToken.username || !decdToken.passwordHash
        || !decdToken.reason || decdToken.reason !== 'NEW_USER') {
        res.status(401).json({ error: 'token missing or invalid' })
        return
    }

    const user = new User({
        email: decdToken.email,
        username: decdToken.username,
        passwordHash: decdToken.passwordHash
    })
    await user.save()

    res.status(200)
        .send({ email: decdToken.email, username: decdToken.username })
})

authRouter.post('/forgotten-password', (req: Request, res: Response) => {
    const body = req.body

    const emailObj = {
        email: body.email,
        reason: 'FORGOTTEN_PASSWORD'
    }
    const emailToken: any = jwt.sign(emailObj, process.env.JWT_SECRET!)

    if (process.env.NODE_ENV === 'test') {
        res.status(200).json({ token: emailToken })
        return
    }

    sendVerificationEmail(
        res, body.email, 'Forgotten password',
        `A password change was requested for email "${body.email}".
Visit the following URL to change your password: ${process.env.WEBSITE_URL}/change-forgotten-password?token=${emailToken}.`)

    res.status(204).end()
})

authRouter.patch('/', async (req, res) => {
    const body = req.body
    const decdToken = verifyTokenFromHeader(req)

    if (!decdToken || !decdToken.email
        || (decdToken.reason !== 'LOGGED_IN'
            && decdToken.reason !== 'FORGOTTEN_PASSWORD')) {
        res.status(401).json({ error: 'token missing or invalid' })
        return
    }

    const pwHash = bcrypt.hashSync(body.password, 10/* saltRounds */)

    await User.findOneAndUpdate(
        { email: decdToken.email }, { passwordHash: pwHash }
    )

    res.status(200).json({ email: decdToken.email })
})

authRouter.delete('/', async (req: Request, res: Response) => {
    const decdToken = verifyTokenFromHeader(req)

    if (!decdToken || !decdToken.email || decdToken.reason !== 'LOGGED_IN') {
        res.status(401).json({ error: 'token missing or invalid' })
        return
    }

    await User.findOneAndRemove({ email: decdToken.email })
    res.status(204).end()
})

export default authRouter
