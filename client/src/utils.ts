import { setNewErrorMessage } from './reducers/notificationReducer'

export function fileToBase64(file: any) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onload = () => resolve(reader.result)
        reader.onerror = error => reject(error)
    })
}

export function binToString(array: Array<number>) {
    let str = ''

    for (let i = 0; i < array.length; i++) {
        str += String.fromCharCode(array[i])}

    return str
}

export function handleAxiosError(
    dispatch: any, err: any, defaultErrStr: string
) {
    dispatch(setNewErrorMessage(
        err.response
            ? err.response.data.error
            : `error: ${defaultErrStr}`
    ))
}
