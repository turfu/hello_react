Hello React
===========
A "hello world" e-commerce application using the MERN stack.

About
-----
This project uses:

- MongoDB (with Mongoose)
- NodeJS, with ExpressJS
- ReactJS
- Redux
- Cypress

Configuration
-------------
The server should be provided the following variables, either as
environment variables, or as key-value pairs in the server/.env file :
```
MONGODB_URI=xxx
JWT_SECRET=xxx
PORT=xxx
# configuration for the SMTP server used for user email verification
# The server should accept TLS connections.
EMAIL_VERIFIER_SMTP_URL=xxx
EMAIL_VERIFIER_SMTP_PORT=xxx
EMAIL_VERIFIER_SMTP_USERNAME=xxx
EMAIL_VERIFIER_SMTP_PASSWORD=xxx
WEBSITE_URL=http://xxx
```

Optional environment variables are:
```
TLS_PRIVATE_KEY_FILE=xxx
TLS_CERTIFICATE_FILE=xxx
```
