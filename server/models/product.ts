import mongoose from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'

mongoose.set('useFindAndModify', false)

interface Product {
    name: string
    category: string
    description: string
    image: {
        data: Buffer
        contentType: string
    }
    price: number
}
export interface ProductSchema extends Product, mongoose.Document {}

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        unique: true,
        required: true
    },
    category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Category',
        required: true
    },
    description: {
        type: String,
        required: true
    },
    image: {
        data: Buffer,
        contentType: String
    },
    price: {
        type: Number,
        required: true
    }
})
productSchema.plugin(uniqueValidator)
productSchema.index({ name: 'text', description: 'text' })

// toJSON is called by JSON.stringify
productSchema.set('toJSON', {
    transform: (_document: any, returnedObject: ProductSchema) => {
        returnedObject.id = returnedObject._id.toString()
        delete returnedObject._id
        delete returnedObject.__v
    }
})

export default mongoose.model<ProductSchema>('Product', productSchema)
