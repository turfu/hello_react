import React, { useState } from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import InputGroup from 'react-bootstrap/InputGroup'
import Table from 'react-bootstrap/Table'
import { useDispatch, useSelector } from 'react-redux'

import { triggerRerenderCategories } from '../reducers/categoryReducer'
import { RootState } from '../reducers/main'
import { setNewSuccessMessage } from '../reducers/notificationReducer'
import categoryService from '../services/categories'
import { Category } from '../types/category'
import { handleAxiosError } from '../utils'

const UpdateCategoryForm: React.FC<{ category: Category }> = ({ category }) => {
    const [newName, setNewName] = useState(category.name)
    const userCookie = useSelector((state: RootState) => state.login.userCookie)
    const dispatch = useDispatch()

    const updateCategory =
        async (event: any, categoryId: string, categoryName: string) => {
        event.preventDefault()

            try {
                await categoryService.update(userCookie!.token, categoryId, categoryName)
            } catch(err) {
                handleAxiosError(dispatch, err, 'categoryService.update()')
                return
            }

            dispatch(triggerRerenderCategories())
            dispatch(setNewSuccessMessage(
                `Successfully renamed category "${categoryName}"`
            ))

            setNewName('')
        }

    return (
        <Form inline
            onSubmit={(event) => updateCategory(event, category.id, newName)}>
            <InputGroup>
                <Form.Control
                    type="text"
                    id="updateCategoryName"
                    value={newName}
                    onChange={(event) => setNewName(event.target.value)} />
                <InputGroup.Append>
                    <Button variant="primary" type="submit"
                        id="updateCategoryButton">
                        rename
                    </Button>
                </InputGroup.Append>
            </InputGroup>
        </Form>
    )
}

function CategoryModifier() {
    const dispatch = useDispatch()
    const categories =
        useSelector((state: RootState) => state.categories.categories)
    const userCookie =
        useSelector((state: RootState) => state.login.userCookie)

    const removeCategory = (categoryId: string, categoryName: string) => {
        categoryService.remove(userCookie!.token, categoryId)

        dispatch(triggerRerenderCategories())
        dispatch(setNewSuccessMessage(`Successfully removed category "${categoryName}"`))
    }

    return (
        <Table striped>
            <tbody>
                {categories.map((cat: Category) =>
                    <tr key={cat.id}>
                        <td>
                            <UpdateCategoryForm category={cat} />
                        </td>
                        <td>
                            <Button variant="danger"
                                onClick={() => removeCategory(cat.id, cat.name)}>
                                remove
                            </Button>
                        </td>
                    </tr>
                )}
            </tbody>
        </Table>
    )
}

export default CategoryModifier
