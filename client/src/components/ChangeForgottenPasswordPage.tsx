import queryString from 'query-string'
import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'

import PasswordChangeForm from './PasswordChangeForm'
import { setNewErrorMessage } from '../reducers/notificationReducer'

function ChangeForgottenPasswordPage() {
    const [httpParams, setHttpParams] = useState<any>(null)
    const dispatch = useDispatch()

    useEffect(() => {
        const httpParameters: any = queryString.parse(window.location.search)
        if (!httpParameters.token)
            dispatch(setNewErrorMessage(
                'no token provided on http parameters'
            ))
        setHttpParams(httpParameters)
    }, [dispatch])

    return (
        <>
            <h1>Change password</h1>
            <br />

            {httpParams && httpParams!.token
                ? <PasswordChangeForm token={httpParams.token} />
                : null}
        </>
    )
}

export default ChangeForgottenPasswordPage
