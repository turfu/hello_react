import { v4 as uuid } from 'uuid'

const ERROR = 'ERROR'
const SUCCESS = 'SUCCESS'
const SET_NEW_ERROR_MESSAGE = 'SET_NEW_ERROR_MESSAGE'
const SET_NEW_SUCCESS_MESSAGE = 'SET_NEW_SUCCESS_MESSAGE'
const POP_MESSAGE = 'POP_MESSAGE'

export interface NotificationState {
    messages: Array<{ data: string; messageType: string; key: string }>
}

const initialState: NotificationState = {
    messages: []
}
export type NotificationReducerState = typeof initialState

const notificationReducer = (state = initialState, action: Action) => {
    switch(action.type) {
    case SET_NEW_ERROR_MESSAGE:
    case SET_NEW_SUCCESS_MESSAGE:
        return { ...state,
            messages: state.messages.concat({
                data: action.data,
                messageType: action.messageType,
                key: uuid()
            }) }
    case POP_MESSAGE: {
        const newMessages = [...state.messages]
        newMessages.shift()
        return { ...state, messages: newMessages }
    } default:
        return state
    }
}

interface SetNewErrorMessageAction {
    type: typeof SET_NEW_ERROR_MESSAGE
    data: string
    messageType: typeof ERROR
}
export const setNewErrorMessage = (msg: string) => {
    return {
        type: SET_NEW_ERROR_MESSAGE,
        data: msg,
        messageType: ERROR
    }
}

interface SetNewSuccessMessageAction {
    type: typeof SET_NEW_SUCCESS_MESSAGE
    data: string
    messageType: typeof SUCCESS
}
export const setNewSuccessMessage = (msg: string) => {
    return {
        type: SET_NEW_SUCCESS_MESSAGE,
        data: msg,
        messageType: SUCCESS
    }
}

interface PopMessageAction {
    type: typeof POP_MESSAGE
}
export const popMessage = () => {
    return {
        type: POP_MESSAGE
    }
}

type Action = SetNewErrorMessageAction
    | SetNewSuccessMessageAction | PopMessageAction

export default notificationReducer
