import { Category } from './category'

export type Product = {
    name: string
    category: Category
    description: string
    image: {
        data: any
        contentType: string
    }
    price: number
    id: string
}

// used in product service
export type ProductObj = {
    name: string
    category: string
    description: string
    image?: {
        data: any
        contentType: string
    }
    price: number
}
