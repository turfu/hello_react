import queryString from 'query-string'
import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { useHistory } from 'react-router-dom'

import {
    setNewErrorMessage, setNewSuccessMessage
} from '../reducers/notificationReducer'
import authService from '../services/auth'
import { handleAxiosError } from '../utils'

function AccountValidator() {
    const dispatch = useDispatch()
    const history = useHistory()

    useEffect(() => {
        const httpParams: any = queryString.parse(window.location.search)
        if (!httpParams.token) {
            dispatch(setNewErrorMessage(
                'no token provided on http parameters'
            ))
            return
        }

        authService.validateAccount(httpParams.token)
            .then(user => {
                dispatch(setNewSuccessMessage(
                    `Successfully validated account for user "${user.username}"`
                ))
                history.push('/auth')
            }).catch(err => {
                handleAxiosError(dispatch, err, 'authService.validateAccount()')
            })
    })

    return <></>
}

export default AccountValidator
