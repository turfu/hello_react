import { Request } from 'express'
import jwt from 'jsonwebtoken'

export function extractAuthTokenFromHeader(request: Request): string | null {
    const auth = request.get('authorization')
    if (auth && auth.toLowerCase().startsWith('bearer ')) {
        return auth.substring(7)
    }

    return null
}

export function verifyTokenFromHeader(request: Request): any {
    const token = extractAuthTokenFromHeader(request)
    if (!token)
        return null

    const decdToken = jwt.verify(token, process.env.JWT_SECRET!)

    return decdToken
}
