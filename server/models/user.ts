import mongoose from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'

interface User {
    username: string
    email: string
    passwordHash?: string
}
export interface UserSchema extends User, mongoose.Document {}

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    passwordHash: {
        type: String,
        required: true
    }
})
userSchema.plugin(uniqueValidator)

// toJSON is called by JSON.stringify
userSchema.set('toJSON', {
    transform: (_document: any, returnedObject: UserSchema) => {
        returnedObject.id = returnedObject._id.toString()
        delete returnedObject._id
        delete returnedObject.__v
        // passwordHash should not be revealed
        delete returnedObject.passwordHash
    }
})

export default mongoose.model<UserSchema>('User', userSchema)
