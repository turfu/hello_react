import axios from 'axios'

function getAll() {
    return axios.get('/api/categories')
        .then(res => res.data)
}

function create(token: string, categoryName: string) {
    const config = {
        headers: { Authorization: `bearer ${token}` },
    }
    // axios automatically sets the Content-Type to application/json
    return axios.post('/api/categories', { name: categoryName }, config)
        .then(res => res.data)
}

function update(token: string, categoryId: string, categoryName: string) {
    const config = {
        headers: { Authorization: `bearer ${token}` },
    }
    // axios automatically sets the Content-Type to application/json
    return axios.patch(`/api/categories/${categoryId}`,
        { name: categoryName }, config)
}

function remove(token: string, categoryId: string) {
    const config = {
        headers: { Authorization: `bearer ${token}` },
    }

    return axios.delete(`/api/categories/${categoryId}`, config)
}

export default {
    getAll,
    create,
    update,
    remove
}
