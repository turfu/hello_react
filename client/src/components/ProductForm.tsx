import React, { useState } from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import InputGroup from 'react-bootstrap/InputGroup'
import { useDispatch, useSelector } from 'react-redux'

import CategoryDropdown from './CategoryDropdown'
import { triggerRerenderArticles } from '../reducers/articleReducer'
import { RootState } from '../reducers/main'
import { setNewSuccessMessage } from '../reducers/notificationReducer'
import productService from '../services/products'
import { fileToBase64, handleAxiosError } from '../utils'

function ProductForm() {
    const [name, setName] = useState('')
    const [categoryId, setCategoryId] = useState('')
    const [description, setDescription] = useState('')
    const [imageFile, setImageFile] = useState<any>(null)
    const [price, setPrice] = useState('')
    const userCookie = useSelector((state: RootState) => state.login.userCookie)
    const dispatch = useDispatch()

    const addProduct = async (event: any) => {
        event.preventDefault()

        try {
            await productService.create(userCookie!.token, {
                name,
                category: categoryId,
                description,
                image: {
                    data: imageFile! === '' ? '' : await fileToBase64(imageFile!),
                    contentType: imageFile!.type
                },
                price: Number(price)
            })
        } catch (err) {
            handleAxiosError(dispatch, err, 'productService.create()')
            return
        }

        setName('')
        setCategoryId('')
        setDescription('')
        setImageFile('')
        setPrice('')

        dispatch(triggerRerenderArticles())
        dispatch(setNewSuccessMessage(
            `Successfully added "${name}" to the website`
        ))
    }

    return (
        <Form onSubmit={addProduct}>
            <Form.Group controlId="productFormName">
                <Form.Label>Name:</Form.Label>
                <Form.Control
                    type="text"
                    value={name}
                    onChange={(event) => setName(event.target.value)} />
            </Form.Group>

            <CategoryDropdown
                id={'productFormCategory'}
                activeCategoryId={categoryId}
                setActiveCategoryId={setCategoryId}
                showAllCategory={false} />

            <Form.Group controlId="productFormDescription">
                <Form.Label>Description:</Form.Label>
                <Form.Control
                    as="textarea"
                    rows={10}
                    value={description}
                    onChange={(event) => setDescription(event.target.value)} />
            </Form.Group>

            <Form.Group controlId="productFormImage">
                <Form.Label>Image:</Form.Label>
                <Form.Control
                    type="file"
                    accept="image/png, image/jpeg"
                    onChange={(event: any) =>
                        setImageFile(event.target.files[0])}/>
            </Form.Group>

            <Form.Group controlId="productFormPrice">
                <Form.Label>Price:</Form.Label>
                <InputGroup>
                    <Form.Control
                        type="text"
                        value={price}
                        onChange={(event) => setPrice(event.target.value)} />
                    <InputGroup.Append>
                        <InputGroup.Text>€</InputGroup.Text>
                    </InputGroup.Append>
                </InputGroup>
            </Form.Group>

            <Button variant="warning" type="submit" id="productFormButton">
                save
            </Button>
        </Form>
    )
}

export default ProductForm
