import React, { useState } from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import { useDispatch, useSelector } from 'react-redux'

import { triggerRerenderCategories } from '../reducers/categoryReducer'
import { RootState } from '../reducers/main'
import { setNewSuccessMessage } from '../reducers/notificationReducer'
import categoryService from '../services/categories'
import { handleAxiosError } from '../utils'

function CategoryForm() {
    const [name, setName] = useState('')
    const userCookie = useSelector((state: RootState) => state.login.userCookie)
    const dispatch = useDispatch()

    const addCategory = async (event: any) => {
        event.preventDefault()

        try {
            await categoryService.create(userCookie!.token, name)
        } catch (err) {
            handleAxiosError(dispatch, err, 'categoryService.create()')
            return
        }

        setName('')

        dispatch(triggerRerenderCategories())
        dispatch(setNewSuccessMessage(
            `Successfully added category "${name}"`
        ))
    }

    return (
        <Form onSubmit={addCategory}>
            <Form.Group controlId="addCategoryName">
                <Form.Label>Name:</Form.Label>
                <Form.Control
                    type="text"
                    value={name}
                    onChange={(event) => setName(event.target.value)} />
            </Form.Group>

            <Button variant="primary" type="submit" id="addCategorySaveButton">
                save
            </Button>
        </Form>
    )
}

export default CategoryForm
