import React, { useState } from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import { useDispatch } from 'react-redux'

import {
    setNewErrorMessage, setNewSuccessMessage
} from '../reducers/notificationReducer'
import authService from '../services/auth'
import { handleAxiosError } from '../utils'

function CreateAccountForm() {
    const [email, setEmail] = useState('')
    const [username, setUsername] = useState('')
    const [password1, setPassword1] = useState('')
    const [password2, setPassword2] = useState('')
    const dispatch = useDispatch()

    const handleCreateAccount = async (event: any) => {
        event.preventDefault()

        if (password1 !== password2) {
            dispatch(setNewErrorMessage('Passwords do not match'))
            return
        }

        try {
            await authService.createUser(username, email, password1)
        } catch (err) {
            handleAxiosError(dispatch, err, 'authService.createUser()')
            return
        }

        dispatch(setNewSuccessMessage(
            `Successfully sent verification email to ${email}`
        ))

        setEmail('')
        setUsername('')
        setPassword1('')
        setPassword2('')
    }

    return (
        <Form onSubmit={handleCreateAccount}>
            <Form.Group controlId="accountUsername">
                <Form.Label>Username:</Form.Label>
                <Form.Control
                    type="text"
                    value={username}
                    onChange={(event) => { setUsername(event.target.value) }}
                />
            </Form.Group>

            <Form.Group controlId="accountEmail">
                <Form.Label>Email:</Form.Label>
                <Form.Control
                    type="text"
                    value={email}
                    onChange={(event) => { setEmail(event.target.value) }}
                />
            </Form.Group>

            <Form.Group controlId="accountPassword1">
                <Form.Label>Password:</Form.Label>
                <Form.Control
                    type="password"
                    value={password1}
                    onChange={(event) => { setPassword1(event.target.value) }}
                />
            </Form.Group>

            <Form.Group controlId="accountPassword2">
                <Form.Label>Confirm password:</Form.Label>
                <Form.Control
                    type="password"
                    value={password2}
                    onChange={(event) => { setPassword2(event.target.value) }}
                />
            </Form.Group>

            <Button variant="primary" type="submit">
                Create account
            </Button>
        </Form>
    )
}

export default CreateAccountForm
