function login(email, password) {
    cy.contains('Login or create account').click()

    cy.get('#loginEmail').type(email)
    cy.get('#loginPassword').type(password)
    cy.get('#loginButton').click()
}

describe('Hello React app authentification', function() {
    beforeEach(function() {
        cy.request('POST', 'http://localhost:3001/api/testing/reset-db')

        cy.addUser('hello', 'hello@hello.com', 'hello')

        cy.visit('http://localhost:3000')
    })

    it('front page can be opened', function() {
        cy.contains('Login or create account')
        cy.contains('Copyright (c) 2020 Florent Bordignon')
    })

    it('user can log in', function() {
        login('hello@hello.com', 'hello')
        cy.contains('logged as hello')
    })

    describe('when logged in', function() {
        beforeEach(function() {
            cy.login('hello@hello.com', 'hello')
            cy.visit('http://localhost:3000')
        })

        it('user can modify its password', function() {
            cy.contains('logged as hello').click()

            cy.contains('change password').click()
            cy.get('#newPassword').type('olleh')
            cy.get('#confirmNewPassword').type('olleh')
            cy.contains('change password').click()

            cy.contains('Logout').click()

            login('hello@hello.com', 'olleh')
            cy.contains('logged as hello')
        })

        it('user can remove its account', function() {
            cy.contains('logged as hello').click()

            cy.contains('remove account').click()
            cy.get('#modalRemoveAccount').click()

            login('hello@hello.com', 'hello')
            cy.contains('logged as hello').should('not.exist')
            cy.contains('Login or create account')
        })
    })
})
