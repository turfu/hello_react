import axios from 'axios'

import { ProductObj } from '../types/product'

function get(
    page: number, perPage: number, categoryId: string, order: string,
    searchStr: string, minPrice: number, maxPrice: number
) {
    let url = '/api/products?'
    // 0 is the id of the 'all' category
    if (categoryId !== '0')
        url += `category=${categoryId}&`
    if (searchStr !== '')
        url += `search=${searchStr}&`
    url += `skip=${perPage * page}&`
    url += `limit=${perPage}&`
    url += `order=${order}`
    if (minPrice !== 0)
        url += `&minPrice=${minPrice}`
    if (maxPrice !== 0)
        url += `&maxPrice=${maxPrice}`

    return axios.get(url)
        .then(res => res.data)
}

function getOne(productId: string) {
    return axios.get(`/api/products/${productId}`)
        .then(res => res.data)
}

function create(token: string, newObject: ProductObj) {
    const config = {
        headers: { Authorization: `bearer ${token}` },
    }
    // axios automatically sets the Content-Type to application/json
    return axios.post('/api/products', newObject, config)
        .then(res => res.data)
}

function update(token: string, productId: string, updateObj: ProductObj) {
    const config = {
        headers: { Authorization: `bearer ${token}` },
    }
    // axios automatically sets the Content-Type to application/json
    return axios.patch(`/api/products/${productId}`, { updateObj }, config)
}

function remove(token: string, productId: string) {
    const config = {
        headers: { Authorization: `bearer ${token}` },
    }

    axios.delete(`/api/products/${productId}`, config)
}

export default {
    get,
    getOne,
    create,
    remove,
    update
}
