import React, { useState } from 'react'
import Button from 'react-bootstrap/Button'
import Col from 'react-bootstrap/Col'
import Form from 'react-bootstrap/Form'
import Image from 'react-bootstrap/Image'
import InputGroup from 'react-bootstrap/InputGroup'
import Row from 'react-bootstrap/Row'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'

import CategoryDropdown from '../components/CategoryDropdown'
import {
    triggerAddToBasket, triggerRerenderArticles
} from '../reducers/articleReducer'
import { RootState } from '../reducers/main'
import { setNewSuccessMessage } from '../reducers/notificationReducer'
import productService from '../services/products'
import { ProductsCookie } from '../types/cookies'
import { Product as ProductType, ProductObj } from '../types/product'
import { binToString, fileToBase64, handleAxiosError } from '../utils'

const ConditionalLink: React.FC<{
    showLink: boolean, to: string, children: React.ReactNode
}> = ({ showLink, to, children }) => {
    if (showLink)
        return (
            <Link to={to}>
                {children}
            </Link>
        )
    else
        return <>{children}</>
}

const Product: React.FC<{
    product: ProductType, limitDescription: boolean,
    showLinkToProductPage: boolean, zoomImgOnHover: boolean
}> = ({
    product, limitDescription, showLinkToProductPage, zoomImgOnHover
}) => {
    const [productName, setProductName] = useState(product.name)
    const [productCategoryId, setProductCategoryId] =
        useState(product.category.id)
    const [productDescription, setProductDescription] =
        useState(product.description)
    const [productImageFile, setProductImageFile] = useState<any>(null)
    const [productPrice, setProductPrice] = useState(product.price)
    const [updateMode, setUpdateMode] = useState(false)
    const userCookie = useSelector((state: RootState) => state.login.userCookie)
    const dispatch = useDispatch()

    const addToBasket = (product: ProductType) => {
        let productStr = window.localStorage.getItem('helloReactProducts')
        let products: ProductsCookie
        if (productStr)
            products = JSON.parse(productStr)
        else
            products = []

        let idx
        for (idx = 0; idx < products.length; ++idx)
            if (products[idx].id === product.id) {
                products[idx].quantity++
                break
            }

        if (idx === products.length)
            products.push({
                name: product.name,
                price: product.price,
                id: product.id,
                quantity: 1
            })

        window.localStorage.setItem(
            'helloReactProducts',
            JSON.stringify(products)
        )

        dispatch(setNewSuccessMessage(`Successfully added "${product.name}" to basket`))
        dispatch(triggerAddToBasket())
    }

    const removeProduct = async (productId: string) => {
        try {
            await productService.remove(userCookie!.token, productId)
        } catch (err) {
            handleAxiosError(dispatch, err, 'productService.remove()')
            return
        }

        dispatch(triggerRerenderArticles())
    }

    const cancelUpdateMode = () => {
        setProductName(product.name)
        setProductCategoryId(product.category.id)
        setProductDescription(product.description)
        setProductImageFile(null)
        setProductPrice(product.price)

        setUpdateMode(false)
    }

    const productButtons = () => (
        <>
            <Button variant="primary"
                onClick={() => addToBasket(product)}>
                add to basket&nbsp;
                <i className="fas fa-cart-plus" />
            </Button>

            {userCookie && userCookie!.username === 'admin'
                ? <>
                    {updateMode
                        ? <Button variant="secondary"
                            onClick={cancelUpdateMode}>
                            cancel updating
                        </Button>
                        : <Button variant="primary"
                            onClick={() => setUpdateMode(true)}>
                            update&nbsp;
                            <i className="fas fa-edit" />
                        </Button>}

                    <Button variant="danger"
                        onClick={() => removeProduct(product.id)}>
                        delete&nbsp;
                        <i className="fas fa-trash-alt" />
                    </Button>
                </>
                : null}
        </>
    )

    const articleFieldsNonUpdateMode = () => (
        <>
            <h2 style={{ marginTop: '16px' }}>
                <ConditionalLink showLink={showLinkToProductPage}
                    to={`/product?productId=${product.id}`}>
                    {product.name}
                </ConditionalLink>
            </h2>
            <hr />

            Category: {product.category.name}
            <hr />

            <div className={limitDescription ? 'limit-7-lines' : ''}
                style={{ textAlign: 'justify', whiteSpace: 'pre-line' }}>
                {product.description}
            </div>
            <hr />

            <div style={{ textAlign: 'center' }}>
                {product.price} €
                &nbsp;&nbsp;|&nbsp;&nbsp;
                {productButtons()}
            </div>
        </>
    )

    const updateProduct = async (event: any) => {
        event.preventDefault()

        let updateObj: ProductObj = { name: productName, category: productCategoryId,
            description: productDescription, price: productPrice }
        if (productImageFile)
            updateObj = { ...updateObj, image: {
                data: await fileToBase64(productImageFile!),
                contentType: productImageFile!.type
            } }

        try {
            await productService.update(
                userCookie!.token, product.id, updateObj
            )
        } catch(err) {
            handleAxiosError(dispatch, err, 'productService.update()')
            return
        }

        if (productImageFile) {
            setProductImageFile('')
        }

        setUpdateMode(false)
        dispatch(triggerRerenderArticles())
    }

    const articleFieldsUpdateMode = () => (
        <>
            <Form onSubmit={updateProduct}>
                <Form.Control
                    type="text"
                    value={productName}
                    onChange={(event) => setProductName(event.target.value)} />

                Category:&nbsp;
                <CategoryDropdown
                    id={'productUpdateCategory'}
                    activeCategoryId={productCategoryId}
                    setActiveCategoryId={setProductCategoryId}
                    showAllCategory={false} />
                <br />

                <Form.Control
                    as="textarea"
                    rows={10}
                    value={productDescription}
                    onChange={(event) => setProductDescription(
                        event.target.value
                    )} />

                <Form.Group controlId="productImage">
                    <Form.Label>Image:&nbsp;</Form.Label>
                    <Form.Control
                        type="file"
                        accept="image/png, image/jpeg"
                        onChange={(event: any) => setProductImageFile(
                            event.target.files[0]
                        )} />
                </Form.Group>

                <Form.Group>
                    <InputGroup>
                        <Form.Control
                            type="text"
                            value={productPrice}
                            onChange={(event) =>
                                setProductPrice(Number(event.target.value))}
                        />
                        <InputGroup.Append>
                            <InputGroup.Text>
                                €
                            </InputGroup.Text>
                        </InputGroup.Append>
                    </InputGroup>
                </Form.Group>

                <Button variant="warning" type="submit">
                    update&nbsp;
                    <i className="fas fa-edit" />
                </Button>

                <div style={{ textAlign: 'center' }}>
                    {productButtons()}
                </div>
            </Form>
        </>
    )

    return (
        <Row>
            <Col md={6} className="d-flex justify-content-center">
                <ConditionalLink showLink={showLinkToProductPage}
                    to={`/product?productId=${product.id}`}>
                    <Image className={zoomImgOnHover ? 'zoom-on-hover' : ''}
                        src={`${binToString(product.image.data.data)}`}
                        alt={product.name} rounded fluid
                        style={{ height: '400px', width: 'auto' }} />
                </ConditionalLink>
            </Col>
            <Col md={6}>
                <div style={{ marginLeft: '10px', marginRight: '10px' }}>
                    {updateMode
                        ? articleFieldsUpdateMode()
                        : articleFieldsNonUpdateMode()}
                </div>
            </Col>
        </Row>
    )
}

export default Product
