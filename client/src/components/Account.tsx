import React, { useState } from 'react'
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'

import PasswordChangeForm from './PasswordChangeForm'
import { triggerLoggedIn, setUserCookie } from '../reducers/loginReducer'
import { RootState } from '../reducers/main'
import { setNewSuccessMessage } from '../reducers/notificationReducer'
import authService from '../services/auth'
import { handleAxiosError } from '../utils'

function Account() {
    const [accountRemovalModalVisible, setAccountRemovalModalVisible]
          = useState(false)
    const [passwordFormVisible, setPasswordFormVisible] = useState(false)
    const userCookie = useSelector((state: RootState) => state.login.userCookie)
    const dispatch = useDispatch()
    const history = useHistory()

    const removeAccount = async () => {
        try {
            await authService.removeUser(userCookie!.token)
        } catch (err) {
            handleAxiosError(dispatch, err, 'authService.removeUser()')
            return
        }

        window.localStorage.removeItem('helloReactUser')

        dispatch(setNewSuccessMessage(
            `Successfully removed user "${userCookie!.username}"`
        ))
        dispatch(setUserCookie(null))

        setAccountRemovalModalVisible(false)
        history.push('/')
        triggerLoggedIn()
    }

    const setPasswordFormVisibleButton = () => {
        if (passwordFormVisible)
            return (
                <Button variant="secondary"
                    onClick={() => setPasswordFormVisible(false)}>
                    cancel changing password
                </Button>
            )
        else
            return (
                <Button variant="warning"
                    onClick={() => setPasswordFormVisible(true)}>
                    change password&nbsp;
                    <i className="fas fa-lock" />
                </Button>
            )
    }

    const accountRemovalModal = () => {
        return (
            <Modal show={accountRemovalModalVisible}nn
                onHide={() => setAccountRemovalModalVisible(false)}>
                <Modal.Header closeButton>
                    <Modal.Title>Warning</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    You are about to delete your account (username {userCookie!.username}).
                </Modal.Body>

                <Modal.Footer >
                    <Button variant="secondary"
                        onClick={() => setAccountRemovalModalVisible(false)}>
                        Close
                    </Button>
                    <Button variant="danger"
                        id="modalRemoveAccount"
                        onClick={removeAccount}>
                        Remove account&nbsp;
                        <i className="fas fa-user-slash" />
                    </Button>
                </Modal.Footer>
            </Modal>
        )
    }

    if (!userCookie)
        return <>Not logged in.</>

    return (
        <>
            <h1>{userCookie.username}&apos;s account</h1>

            {setPasswordFormVisibleButton()}
            <br />

            <div style={{ display: passwordFormVisible ? '' : 'none' }}>
                <PasswordChangeForm token={userCookie!.token} />
            </div>
            <br />

            {accountRemovalModal()}

            <Button variant="danger"
                onClick={() => setAccountRemovalModalVisible(true)}>
                remove account&nbsp;
                <i className="fas fa-user-slash" />
            </Button>
        </>
    )
}

export default Account
