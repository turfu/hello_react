import React, { useState } from 'react'
import Button from 'react-bootstrap/Button'
import { useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'

import CreateAccountForm from './CreateAccountForm'
import LoginForm from './LoginForm'
import { RootState } from '../reducers/main'

function Auth() {
    const [loginEnabled, setLoginEnabled] = useState(true)
    const [loginButtonText, setLoginButtonText] =
          useState('Create a new account')
    const userCookie = useSelector((state: RootState) => state.login.userCookie)
    const history = useHistory()

    const toggleLoginEnabled = () => {
        if (loginEnabled)
            setLoginButtonText('Login')
        else
            setLoginButtonText('Create a new account')

        setLoginEnabled(!loginEnabled)
    }

    return (
        <div style={{ display: userCookie === null ? '' : 'none' }}>
            <h1>{loginEnabled ? 'Login' : 'Create a new account'}
            </h1>

            {loginEnabled
                ? <>
                    <LoginForm />
                    <Button variant="link"
                        onClick={() => history.push('/forgotten-password')}>
                        password forgotten?
                    </Button>
                </>
                : <CreateAccountForm />}
            <br />

            <Button variant="link" onClick={toggleLoginEnabled}>
                {loginButtonText}
            </Button>
        </div>
    )
}

export default Auth
