import React, { useEffect, useState } from 'react'
import Dropdown from 'react-bootstrap/Dropdown'
import Spinner from 'react-bootstrap/Spinner'
import { useSelector } from 'react-redux'

import { RootState } from '../reducers/main'
import { Category } from '../types/category'

const CategoryDropdown: React.FC<{
    id: string, activeCategoryId: string,
    setActiveCategoryId: (id: string) => void,
    showAllCategory: boolean
}> = ({id, activeCategoryId, setActiveCategoryId, showAllCategory}) => {
    const [showSpinner, setShowSpinner] = useState(true)
    const [activeCategoryName, setActiveCategoryName] = useState('')
    const categories =
        useSelector((state: RootState) => state.categories.categories)

    useEffect(() => {
        setShowSpinner(categories.length === 0)

        if (showAllCategory && activeCategoryId === '0') {
            setActiveCategoryName('All')
            return
        }

        for (const category of categories)
            if (category.id === activeCategoryId) {
                setActiveCategoryName(category.name)
                return
            }

        setActiveCategoryName('')
    }, [categories, activeCategoryId, showAllCategory])

    return (
        <Dropdown id={id}>
            <Dropdown.Toggle variant="primary" id="dropdown-basic">
                Category: {activeCategoryName}
            </Dropdown.Toggle>

            <Dropdown.Menu>
                {showAllCategory
                    ? // '0' === category id for 'ALL'
                    <Dropdown.Item active={activeCategoryId === '0'}
                        onClick={() => setActiveCategoryId('0')}>
                        All
                    </Dropdown.Item>
                    : null}

                <div style={{ display: showSpinner ? '' : 'none' }}>
                    <Spinner animation="border" />
                </div>

                {categories.map((cat : Category) =>
                    <Dropdown.Item
                        key={cat.id}
                        active={cat.id === activeCategoryId}
                        onClick={() => setActiveCategoryId(cat.id)}>
                        {cat.name}
                    </Dropdown.Item>
                )}
            </Dropdown.Menu>
        </Dropdown>
    )
}

export default CategoryDropdown
