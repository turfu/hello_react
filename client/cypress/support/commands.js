// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add('addUser', (username, email, password) => {
    cy.request(
        'POST', 'http://localhost:3001/api/auth/new-user',
        { username, email, password }
    ).then(res =>
        cy.request(
            'POST',
            'http://localhost:3001/api/auth/validate-email',
            { emailedToken: res.body.token }
        )
    )
})

Cypress.Commands.add('login', (email, password) => {
    cy.request(
        'POST',
        'http://localhost:3001/api/auth/login',
        { email, password }
    ).then(res => {
        window.localStorage.setItem('helloReactUser', JSON.stringify(res.body))
    })
})

Cypress.Commands.add('createCategory', (name) => {
    const userCookie = window.localStorage.getItem('helloReactUser')

    cy.request({
        auth: { bearer: JSON.parse(userCookie).token },
        method: 'POST',
        url: 'http://localhost:3001/api/categories',
        body: { name }
    }).then((res) => {
        cy.setCookie(`helloReactCategory${name}`, res.body.id)
    })
})

Cypress.Commands.add(
    'createProduct', (name, categoryName, description, price) => {
        const userCookie = window.localStorage.getItem('helloReactUser')
        cy.getCookie(`helloReactCategory${categoryName}`).then(categoryId => {
            cy.request({
                auth: { bearer: JSON.parse(userCookie).token },
                method: 'POST',
                url: 'http://localhost:3001/api/products',
                body: {
                    name,
                    category: categoryId.value,
                    description,
                    image: {
                        data: '',
                        contentType: ''
                    },
                    price
                }
            })
        })
    })
