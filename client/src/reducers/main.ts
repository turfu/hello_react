import { combineReducers } from 'redux'

import articleReducer, { ArticleReducerState } from './articleReducer'
import categoryReducer, { CategoryReducerState } from './categoryReducer'
import loginReducer, { LoginReducerState } from './loginReducer'
import notificationReducer, {
    NotificationReducerState
} from './notificationReducer'

export const rootReducer = combineReducers({
    articles: articleReducer,
    categories: categoryReducer,
    login: loginReducer,
    notifications: notificationReducer
})

export type RootState = {
    articles: ArticleReducerState
    categories: CategoryReducerState
    login: LoginReducerState
    notifications: NotificationReducerState
}
