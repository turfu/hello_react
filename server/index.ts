import dotenv from 'dotenv'
import fs from 'fs'
import http from 'http'
import https from 'https'

dotenv.config()

import app from './app'

let server
if (process.env.TLS_PRIVATE_KEY_FILE) {
    const key = fs.readFileSync(process.env.TLS_PRIVATE_KEY_FILE!)
    const cert = fs.readFileSync(process.env.TLS_CERTIFICATE_FILE!)

    server = https.createServer({ key, cert }, app)
} else {
    server = http.createServer(app)
}

server.listen(process.env.PORT, () => {
    console.log(`Server running on port ${process.env.PORT}`)
})
