import queryString from 'query-string'
import { Range } from 'rc-slider'
import 'rc-slider/assets/index.css'
import React, { useEffect, useState } from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import InputGroup from 'react-bootstrap/InputGroup'
import Pagination from 'react-js-pagination'
import Spinner from 'react-bootstrap/Spinner'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'

import CategoryDropdown from '../components/CategoryDropdown'
import CategoryForm from '../components/CategoryForm'
import CategoryModifier from '../components/CategoryModifier'
import OrderDropdown from '../components/OrderDropdown'
import Product from '../components/Product'
import ProductForm from '../components/ProductForm'
import { triggerRerenderArticles } from '../reducers/articleReducer'
import { RootState } from '../reducers/main'
import { setCategories } from '../reducers/categoryReducer'
import categoryService from '../services/categories'
import productService from '../services/products'
import { Product as ProductType } from '../types/product'
import { handleAxiosError } from '../utils'

function Articles() {
    const perPage = 6
    // '0' === category id for 'ALL'
    const [activeCategoryId, setActiveCategoryId] = useState('0')
    const [articlesToShow, setArticlesToShow] = useState<Array<ProductType>>([])
    const [cheapest, setCheapest] = useState(0)
    const [mostExpensive, setMostExpensive] = useState(0)
    const [doSearchTrigger, setDoSearchTrigger] = useState(false)
    const [httpParamsLoaded, setHttpParamsLoaded] = useState(false)
    const [minPrice, setMinPrice] = useState(0)
    const [maxPrice, setMaxPrice] = useState(0)
    const [order, setOrder] = useState('NAME')
    const [page, setPage] = useState(1)
    const [prevSearchStr, setPrevSearchStr] = useState('')
    const [searchStr, setSearchStr] = useState('')
    const [skipNextEffect, setSkipNextEffect] = useState(false)
    const [showAddNewProduct, setShowAddNewProduct] = useState(false)
    const [showCategoryForm, setShowCategoryForm] = useState(false)
    const [showModifyCategories, setShowModifyCategories] = useState(false)
    const [showSpinner, setShowSpinner] = useState(false)
    const [totalArticles, setTotalArticles] = useState(0)
    const rerenderArticlesTrigger =
        useSelector((state: RootState) => state.articles.rerenderArticlesTrigger)
    const rerenderCategoriesTrigger =
        useSelector((state: RootState) => state.categories.rerenderCategoriesTrigger)
    const userCookie =
        useSelector((state: RootState) => state.login.userCookie)
    const dispatch = useDispatch()
    const history = useHistory()

    useEffect(() => {
        const httpParams = queryString.parse(window.location.search)

        if (httpParams.page)
            setPage(Number(httpParams.page))

        if (httpParams.activeCategoryId)
            setActiveCategoryId(String(httpParams.activeCategoryId))

        if (httpParams.order)
            setOrder(String(httpParams.order))

        if (httpParams.searchStr) {
            setSearchStr(String(httpParams.searchStr))
            setPrevSearchStr(String(httpParams.searchStr))
            setDoSearchTrigger(true)
        }

        if (httpParams.minPrice)
            setMinPrice(Number(httpParams.minPrice))
        if (httpParams.maxPrice)
            setMaxPrice(Number(httpParams.maxPrice))

        setHttpParamsLoaded(true)
        // execute this effect only once
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        if (skipNextEffect) {
            setSkipNextEffect(false)
            return
        }

        if (!httpParamsLoaded)
            return

        let thePage = page
        let thePrevSearchStr = prevSearchStr
        if (doSearchTrigger) {
            setPrevSearchStr(searchStr)
            thePrevSearchStr = searchStr

            setPage(1)
            thePage = 1

            setDoSearchTrigger(false)
            setSkipNextEffect(true)
        }

        setShowSpinner(true)

        let url = `/?page=${thePage}`
        url += `&activeCategoryId=${activeCategoryId}`
        url += `&order=${order}`
        if (thePrevSearchStr !== '')
            url += `&searchStr=${thePrevSearchStr}`
        if (minPrice !== 0)
            url += `&minPrice=${minPrice}`
        if (maxPrice !== 0)
            url += `&maxPrice=${maxPrice}`
        history.push(url)

        productService.get(
            thePage - 1, perPage, activeCategoryId, order, thePrevSearchStr,
            minPrice, maxPrice
        ).then(productObj => {
            setArticlesToShow(productObj.products)
            setTotalArticles(productObj.count)

            setCheapest(productObj.cheapest)
            if (minPrice === 0)
                setMinPrice(productObj.cheapest)

            setMostExpensive(productObj.mostExpensive)
            if (maxPrice === 0)
                setMaxPrice(productObj.mostExpensive)

            setShowSpinner(false)
        }).catch(err => {
            handleAxiosError(dispatch, err, 'productService.get()')
        })
        // do not execute this effect if searchStr or skipNextEffect changes
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [httpParamsLoaded, order, page, rerenderArticlesTrigger,
        activeCategoryId, doSearchTrigger,
        minPrice, maxPrice,
        dispatch, history])

    useEffect(() => {
        categoryService.getAll()
            .then((categories) => {
                dispatch(setCategories(categories))
            }).catch(err => {
                handleAxiosError(dispatch, err, 'categoryService.getAll()')
            })
    }, [rerenderCategoriesTrigger, dispatch])

    const doSetDoSearch = (event: any) => {
        event.preventDefault()

        setDoSearchTrigger(true)
        dispatch(triggerRerenderArticles())
    }

    const articleSelector = () => (
        <>
            <div style={{ float: 'left' }}>
                <CategoryDropdown
                    id={'articleSelectorCategory'}
                    activeCategoryId={activeCategoryId}
                    setActiveCategoryId={setActiveCategoryId}
                    showAllCategory={true} />
            </div>

            <div style={{ float: 'left' }}>
                <OrderDropdown order={order} setOrder={setOrder} />
            </div>

            <div style={{ float: 'left' }}>
                <Form onSubmit={doSetDoSearch} inline>
                    <Form.Group controlId="searchStr">
                        <Form.Label>&nbsp;Search:&nbsp;</Form.Label>
                        <InputGroup>
                            <Form.Control
                                type="text"
                                value={searchStr}
                                onChange={(event) => setSearchStr(event.target.value)} />

                            <InputGroup.Append>
                                <Button variant="primary" type="submit">
                                    <i className="fas fa-search" />
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Form.Group>
                </Form>
            </div>

            <div style={{ clear: 'left', marginBottom: '10px' }}>
            </div>

            Price range: [{minPrice}, {maxPrice}] €
            {mostExpensive > 0
                // @ts-ignore
                ? <Range
                    min={cheapest}
                    max={mostExpensive}
                    defaultValue={[cheapest, mostExpensive]}
                    onChange={(p: Array<number>) => {
                        setMinPrice(p[0]); setMaxPrice(p[1])}} />
                : null}
        </>
    )

    const adminButtons = () => {
        const elements = []

        if (!userCookie || userCookie.username !== 'admin')
            return <></>

        if (showAddNewProduct)
            elements.push(
                <Button key="1" variant="secondary"
                    onClick={() => setShowAddNewProduct(false)}>
                    Cancel adding a new product
                </Button>
            )
        else
            elements.push(
                <Button key="1" variant="primary"
                    onClick={() => setShowAddNewProduct(true)}>
                    Add a new product
                </Button>
            )

        if (showCategoryForm)
            elements.push(
                <Button key="2" variant="secondary"
                    onClick={() => setShowCategoryForm(false)}>
                    Cancel adding a new category
                </Button>
            )
        else
            elements.push(
                <Button key="2" variant="primary"
                    onClick={() => setShowCategoryForm(true)}>
                    Add a new category
                </Button>
            )

        if (showModifyCategories)
            elements.push(
                <Button key="3" variant="secondary"
                    onClick={() => setShowModifyCategories(false)}>
                    Cancel modifying categories
                </Button>
            )
        else
            elements.push(
                <Button key="3" variant="primary"
                    onClick={() => setShowModifyCategories(true)}>
                    Modify categories
                </Button>
            )

        return elements
    }

    const adminForms = () => {
        if (!userCookie || userCookie.username !== 'admin')
            return <></>

        return (
            <>
                <div style={{ display: showAddNewProduct ? '' : 'none' }}>
                    <h2>Add a new product</h2>
                    <ProductForm />
                </div>

                <div style={{ display: showCategoryForm ? '' : 'none' }}>
                    <h2>Add a new category</h2>
                    <CategoryForm />
                </div>

                <div style={{ display: showModifyCategories ? '' : 'none' }}>
                    <h2>Modify categories</h2>
                    <CategoryModifier />
                </div>
            </>
        )
    }

    return (
        <>
            <div style={{ display: showSpinner ? '' : 'none',
                position: 'fixed', top: '50%', left: '50%',
                zIndex: 100 }}>
                <Spinner animation="border" />
            </div>

            {articleSelector()}
            <br />

            {prevSearchStr
                ? <h2>Search results for &quot;{prevSearchStr}&quot;:</h2>
                : null}

            <div className="product-grid-container">
                {articlesToShow.map(article =>
                    <div key={article.id} className="grid-item article-item">
                        <Product
                            product={article}
                            limitDescription={true}
                            showLinkToProductPage={true}
                            zoomImgOnHover={true} />
                    </div>
                )}
            </div>
            <br />

            <Pagination
                itemClass="page-item"
                linkClass="page-link"
                activePage={page}
                itemsCountPerPage={perPage}
                totalItemsCount={totalArticles}
                pageRangeDisplayed={5}
                onChange={(newPage) => setPage(newPage)}
            />

            {adminButtons()}
            {adminForms()}
        </>
    )
}

export default Articles
