import axios from 'axios'

function login(email: string, password: string) {
    // axios automatically sets the Content-Type to application/json
    return axios.post('/api/auth/login', { email, password })
        .then(res => res.data)
}

function createUser(username: string, email: string, password: string) {
    // axios automatically sets the Content-Type to application/json
    return axios.post('/api/auth/new-user', { username, email, password })
        .then(res => res.data)
}

function validateAccount(emailedToken: string) {
    // axios automatically sets the Content-Type to application/json
    return axios.post('/api/auth/validate-email', { emailedToken })
        .then(res => res.data)
}

function forgottenPassword(email: string) {
    // axios automatically sets the Content-Type to application/json
    return axios.post('/api/auth/forgotten-password', { email })
        .then(res => res.data)
}

function changePassword(token: string, password: string) {
    const config = {
        headers: { Authorization: `bearer ${token}` },
    }
    // axios automatically sets the Content-Type to application/json
    return axios.patch('/api/auth/', { password }, config)
        .then(res => res.data)
}

function removeUser(token: string) {
    const config = {
        headers: { Authorization: `bearer ${token}` },
    }

    return axios.delete('/api/auth/', config)
}

export default {
    login,
    createUser,
    changePassword,
    forgottenPassword,
    removeUser,
    validateAccount
}
