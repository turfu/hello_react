import { Category } from '../types/category'

const SET_CATEGORIES = 'SET_CATEGORIES'
const TRIGGER_RERENDER_CATEGORIES = 'TRIGGER_RERENDER_CATEGORIES'

export type CategoryReducerState = {
    categories: Array<Category>
    rerenderCategoriesTrigger: boolean
}
const initialState: CategoryReducerState = {
    categories: [],
    rerenderCategoriesTrigger: false
}

const categoryReducer = (state = initialState, action: Action) => {
    switch(action.type) {
    case SET_CATEGORIES:
        return { ...state, categories: action.data }
    case TRIGGER_RERENDER_CATEGORIES:
        return { ...state,
            rerenderCategoriesTrigger: !state.rerenderCategoriesTrigger }
    default:
        return state
    }
}

interface SetCategoriesAction {
    type: typeof SET_CATEGORIES
    data: Array<Category>
}
export const setCategories = (categories: Array<Category>) => {
    return {
        type: SET_CATEGORIES,
        data: categories
    }
}

interface TriggerRerenderCategoriesAction {
    type: typeof TRIGGER_RERENDER_CATEGORIES
}
export const triggerRerenderCategories = () => {
    return {
        type: TRIGGER_RERENDER_CATEGORIES
    }
}

type Action = SetCategoriesAction | TriggerRerenderCategoriesAction

export default categoryReducer
