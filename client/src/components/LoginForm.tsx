import React, { useState } from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import { useDispatch } from 'react-redux'
import { useHistory } from 'react-router-dom'

import { triggerLoggedIn } from '../reducers/loginReducer'
import { setNewSuccessMessage } from '../reducers/notificationReducer'
import authService from '../services/auth'
import { handleAxiosError } from '../utils'

function LoginForm() {
    const history = useHistory()
    const [password, setPassword] = useState('')
    const [email, setEmail] = useState('')
    const dispatch = useDispatch()

    const handleLogin = async (event: any) => {
        event.preventDefault()

        let user
        try {
            user = await authService.login(email, password)
        } catch (err) {
            handleAxiosError(dispatch, err, 'authService.login()')
            return
        }

        window.localStorage.setItem(
            'helloReactUser', JSON.stringify(user)
        )

        dispatch(setNewSuccessMessage(
            `Successfully logged as ${user.username}`
        ))
        setEmail('')
        setPassword('')

        history.push('/')
        dispatch(triggerLoggedIn())
    }

    return (
        <Form onSubmit={handleLogin}>
            <Form.Group controlId="loginEmail">
                <Form.Label>Email:</Form.Label>
                <Form.Control
                    type="text"
                    value={email}
                    onChange={(event) => { setEmail(event.target.value) }}
                />
            </Form.Group>

            <Form.Group controlId="loginPassword">
                <Form.Label>Password:</Form.Label>
                <Form.Control
                    type="password"
                    value={password}
                    onChange={(event) => { setPassword(event.target.value) }}
                />
            </Form.Group>

            <Button variant="primary" type="submit" id="loginButton">
                Login
            </Button>
        </Form>
    )
}

export default LoginForm
