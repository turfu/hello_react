import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { useHistory } from 'react-router-dom'

import { triggerLoggedIn, setUserCookie } from '../reducers/loginReducer'

function Logout() {
    const history = useHistory()
    const dispatch = useDispatch()

    useEffect(() => {
        window.localStorage.removeItem('helloReactUser')
        dispatch(setUserCookie(null))
        history.push('/')
        triggerLoggedIn()
    })

    return <></>
}

export default Logout
