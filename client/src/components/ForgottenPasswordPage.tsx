import React, { useState } from 'react'
import Form from 'react-bootstrap/Form'
import { useDispatch } from 'react-redux'

import { setNewSuccessMessage } from '../reducers/notificationReducer'
import authService from '../services/auth'
import { handleAxiosError } from '../utils'

function ForgottenPasswordPage() {
    const [email, setEmail] = useState('')
    const dispatch = useDispatch()

    const forgottenPassword = (event: any) => {
        event.preventDefault()

        authService.forgottenPassword(email)
            .then(() => {
                dispatch(setNewSuccessMessage(
                    `Successfully send password update email at "${email}"`
                ))
            }).catch(err => {
                handleAxiosError(dispatch, err,
                    'authService.forgottenPassword()')
            })
    }

    return (
        <>
            <h1>Password forgotten</h1>
            <br />

            <Form onSubmit={forgottenPassword}>
                <Form.Group controlId="forgottenPwEmail">
                    <Form.Label>
                        Enter your email to receive a link to update your password:
                    </Form.Label>
                    <Form.Control
                        type="text"
                        value={email}
                        onChange={(event) => setEmail(event.target.value)} />
                </Form.Group>
            </Form>
        </>
    )
}

export default ForgottenPasswordPage
