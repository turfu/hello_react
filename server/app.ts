import cors from 'cors'
import express from 'express'
import 'express-async-errors'
import mongoose from 'mongoose'

import authRouter from './controllers/auth'
import categoryRouter from './controllers/categories'
import productRouter from './controllers/products'
import testingRouter from './controllers/testing'
import {
    requestLogger, unknownEndpoint, errorHandler
} from './utils/middleware'

function mongoose_connect() {
    if (process.env.MONGODB_USER!) {
        return mongoose.connect(process.env.MONGODB_URI!,
            {
                useNewUrlParser: true,
                useUnifiedTopology: true,
                authSource: 'admin',
                user: process.env.MONGODB_USER!,
                pass: process.env.MONGODB_PASSWORD!
            })
    } else {
        return mongoose.connect(process.env.MONGODB_URI!,
            {
                useNewUrlParser: true,
                useUnifiedTopology: true
            })
    }
}

mongoose_connect()
    .then(() => {
        console.log('connected to MongoDB')
    })
    .catch((_error) => {
        console.error('error connecting to MongoDB', _error.message)
    })

const app = express()

app.use(cors())
app.use(express.static('client_build'))
app.use(express.json({ limit: '10mb' }))
app.use(requestLogger)

app.use('/api/auth', authRouter)
app.use('/api/categories', categoryRouter)
app.use('/api/products', productRouter)

if (process.env.NODE_ENV === 'test')
    app.use('/api/testing', testingRouter)

const clientPaths = [
    'account', 'auth', 'basket', 'change-forgotten-password',
    'forgotten-password', 'logout', 'product', 'validate-account'
]
for (const path of clientPaths)
    app.get(`/${path}`, (_req, res) => {
        res.sendFile(`${__dirname}/build/index.html`)
    })

app.use(unknownEndpoint)
app.use(errorHandler)

export default app
