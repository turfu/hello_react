import express, { Request, Response } from 'express'

import Category from '../models/category'
import { verifyTokenFromHeader } from '../utils/auth'

const categoriesRouter = express.Router()

categoriesRouter.get('/', async (_req: Request, res: Response) => {
    const categories = await Category.find({}, null)
        .sort('name')

    res.json(categories.map(category => category.toJSON()))
})

categoriesRouter.patch('/:id', async (req: Request, res: Response) => {
    const body = req.body
    const decdToken = verifyTokenFromHeader(req)

    if (!decdToken || decdToken.reason !== 'LOGGED_IN') {
        res.status(401).json({ error: 'token missing or invalid' })
        return
    }
    if (decdToken.username !== 'admin') {
        res.status(401).json({ error: 'only admin can update a category' })
        return
    }

    if (body.name)
        await Category.findByIdAndUpdate(req.params.id, { name: body.name })

    res.status(200).end()
})

categoriesRouter.post('/', async (req: Request, res: Response) => {
    const body = req.body
    const decdToken = verifyTokenFromHeader(req)

    if (!decdToken || decdToken.reason !== 'LOGGED_IN') {
        res.status(401).json({ error: 'token missing or invalid' })
        return
    }
    if (decdToken.username !== 'admin') {
        res.status(401).json({ error: 'only admin can create a category' })
        return
    }

    const category = new Category({ name: body.name })
    const savedCategory = await category.save()

    res.status(200).json({ id: savedCategory.id })
})

categoriesRouter.delete('/:id', async (req: Request, res: Response) => {
    const decdToken = verifyTokenFromHeader(req)

    if (!decdToken || decdToken.reason !== 'LOGGED_IN') {
        res.status(401).json({ error: 'token missing or invalid' })
        return
    }
    if (decdToken.username !== 'admin') {
        res.status(401).json({ error: 'only admin can delete a category' })
        return
    }

    await Category.findByIdAndRemove(req.params.id)
    res.status(204).end()
})

export default categoriesRouter
