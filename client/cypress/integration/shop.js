describe('Hello React shopping', function() {
    beforeEach(function() {
        cy.request('POST', 'http://localhost:3001/api/testing/reset-db')
        cy.addUser('admin', 'admin@admin.com', 'admin')
    })

    describe('when logged in as admin', function() {
        beforeEach(function() {
            cy.login('admin@admin.com', 'admin')
            cy.visit('http://localhost:3000')
        })

        it('user can add a category and modify it', function() {
            cy.contains('Add a new category').click()
            cy.get('#addCategoryName').type('helloIAmACategory')
            cy.get('#addCategorySaveButton').click()
            cy.get('#successToast').click()

            cy.contains('Category: All').click()
            cy.contains('helloIAmACategory')

            cy.contains('Modify categories').click()
            cy.get('#updateCategoryName').type(
                '{backspace}'.repeat('helloIAmACategory'.length)
                + 'helloIAmAModifiedCategory'
            )
            cy.get('#updateCategoryButton').click()
            cy.get('#successToast').click()

            cy.contains('Category: All').click()
            cy.contains('helloIAmAModifiedCategory')
        })

        describe('when there is a category', function() {
            beforeEach(function() {
                cy.createCategory('book')
                cy.visit('http://localhost:3000')
            })

            it('user can add a product', function() {
                cy.contains('Add a new product').click()
                cy.get('#productFormName').type('hello')
                cy.get('#productFormCategory').find('button').click()
                cy.get('#productFormCategory').children()
                    .contains('book').click()
                cy.get('#productFormDescription').type(
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
                )
                cy.get('#productFormPrice').type('10')
                cy.get('#productFormButton').click()

                cy.contains(
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
                )
            })
        })

        describe('when there is a product', function() {
            beforeEach(function() {
                cy.createCategory('book')
                cy.createProduct(
                    'hello product', 'book',
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                    '5'
                )
                cy.visit('http://localhost:3000')
            })

            it('user can add a product to basket and remove it', function() {
                cy.contains('add to basket').click()
                cy.contains('add to basket').click()
                cy.contains('add to basket').click()
                cy.get('#successToast').click()
                cy.get('#successToast').click()
                cy.get('#successToast').click()

                cy.get('#basketLink').click()

                cy.contains('x3')
                cy.contains('15 €')
                cy.contains('Total: 15 €')

                cy.get('#basketMinus').click()
                cy.contains('x2')
                cy.contains('10 €')
                cy.contains('Total: 10 €')

                cy.get('#basketRemoveButton').click()
                cy.contains('Total: 0 €')
            })
        })
    })
})
