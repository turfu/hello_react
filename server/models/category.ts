import mongoose from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'

mongoose.set('useFindAndModify', false)

interface Category {
    name: string
    products: string
}
export interface CategorySchema extends Category, mongoose.Document {}

const categorySchema = new mongoose.Schema({
    name: {
        type: String,
        unique: true,
        required: true
    },
    products: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Product'
        }
    ],
})
categorySchema.plugin(uniqueValidator)

// toJSON is called by JSON.stringify
categorySchema.set('toJSON', {
    transform: (_document: any, returnedObject: CategorySchema) => {
        returnedObject.id = returnedObject._id.toString()
        delete returnedObject._id
        delete returnedObject.__v
    }
})

export default mongoose.model<CategorySchema>('Category', categorySchema)
