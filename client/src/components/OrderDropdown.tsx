import React, { useEffect, useState } from 'react'
import Dropdown from 'react-bootstrap/Dropdown'

const OrderDropdown:
React.FC<{ order: string, setOrder: (order: string) => void }> = ({
    order, setOrder
}) => {
    const [orderName, setOrderName] = useState('')

    useEffect(() => {
        switch (order) {
        case 'NAME':
            setOrderName('Name')
            break
        case 'NAME_DESC':
            setOrderName('Name (descending)')
            break
        case 'PRICE':
            setOrderName('Price')
            break
        case 'PRICE_DESC':
            setOrderName('Price (descending)')
            break
        default:
            break
        }
    }, [order, setOrderName])

    return (
        <Dropdown>
            <Dropdown.Toggle variant="primary" id="dropdown-basic">
                Order by: {orderName}
            </Dropdown.Toggle>

            <Dropdown.Menu>
                <Dropdown.Item active={order === 'NAME'}
                    onClick={() => setOrder('NAME')}>
                    Name
                </Dropdown.Item>

                <Dropdown.Item active={order === 'NAME_DESC'}
                    onClick={() => setOrder('NAME_DESC')}>
                    Name (descending)
                </Dropdown.Item>

                <Dropdown.Item active={order === 'PRICE'}
                    onClick={() => setOrder('PRICE')}>
                    Price
                </Dropdown.Item>

                <Dropdown.Item active={order === 'PRICE_DESC'}
                    onClick={() => setOrder('PRICE_DESC')}>
                    Price (descending)
                </Dropdown.Item>
            </Dropdown.Menu>
        </Dropdown>
    )
}

export default OrderDropdown
