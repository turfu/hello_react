import { UserCookie } from '../types/cookies'

const SET_USER_COOKIE = 'SET_USER_COOKIE'
const SET_TMP_USERNAME = 'SET_TMP_USERNAME'
const SET_TMP_PASSWORD = 'SET_TMP_PASSWORD'
const TRIGGER_LOGGED_IN = 'TRIGGER_LOGGED_IN'

export type LoginReducerState = {
    userCookie: UserCookie | null,
    tmpUsername: string,
    tmpPassword: string,
    loggedInTrigger: boolean
}
const initialState: LoginReducerState = {
    userCookie: null,
    tmpUsername: '',
    tmpPassword: '',
    loggedInTrigger: false
}

const loginReducer = (state = initialState, action: Action) => {
    switch(action.type) {
    case SET_USER_COOKIE:
        return { ...state, userCookie: action.data }
    case SET_TMP_USERNAME:
        return { ...state, tmpUsername: action.data }
    case SET_TMP_PASSWORD:
        return { ...state, tmpPassword: action.data }
    case TRIGGER_LOGGED_IN:
        return { ...state, loggedInTrigger: !state.loggedInTrigger }
    default:
        return state
    }
}

interface SetUserCookieAction {
    type: typeof SET_USER_COOKIE
    data: UserCookie | null
}
export const setUserCookie = (cookie: UserCookie | null) => {
    return {
        type: SET_USER_COOKIE,
        data: cookie
    }
}

interface SetTmpUsernameAction {
    type: typeof SET_TMP_USERNAME
    data: string
}
export const setTmpUsername = (uname: string) => {
    return {
        type: SET_TMP_USERNAME,
        data: uname
    }
}

interface SetTmpPasswordAction {
    type: typeof SET_TMP_PASSWORD
    data: string
}
export const setTmpPassword = (pw: string) => {
    return {
        type: SET_TMP_PASSWORD,
        data: pw
    }
}

interface TriggerLoggedInAction {
    type: typeof TRIGGER_LOGGED_IN
}
export const triggerLoggedIn = () => {
    return {
        type: TRIGGER_LOGGED_IN
    }
}

type Action = SetUserCookieAction | SetTmpUsernameAction
    | SetTmpPasswordAction | TriggerLoggedInAction

export default loginReducer
