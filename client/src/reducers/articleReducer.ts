const TRIGGER_ADD_TO_BASKET = 'TRIGGER_ADD_TO_BASKET'
const TRIGGER_RERENDER_ARTICLES = 'TRIGGER_RERENDER_ARTICLES'

export interface ArticleState {
    addToBasketTrigger: boolean,
    rerenderArticlesTrigger: boolean
}

const initialState: ArticleState = {
    addToBasketTrigger: false,
    rerenderArticlesTrigger: false
}
export type ArticleReducerState = typeof initialState

const articleReducer = (state = initialState, action: Action) => {
    switch(action.type) {
    case TRIGGER_ADD_TO_BASKET:
        return { ...state, addToBasketTrigger: !state.addToBasketTrigger }
    case TRIGGER_RERENDER_ARTICLES:
        return { ...state,
            rerenderArticlesTrigger: !state.rerenderArticlesTrigger }
    default:
        return state
    }
}

interface TriggerAddToBasketAction {
    type: typeof TRIGGER_ADD_TO_BASKET
}
export const triggerAddToBasket = () => {
    return {
        type: TRIGGER_ADD_TO_BASKET
    }
}

interface TriggerRerenderArticlesAction {
    type: typeof TRIGGER_RERENDER_ARTICLES
}
export const triggerRerenderArticles = () => {
    return {
        type: TRIGGER_RERENDER_ARTICLES
    }
}

type Action = TriggerAddToBasketAction | TriggerRerenderArticlesAction

export default articleReducer
