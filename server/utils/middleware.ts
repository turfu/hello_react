import { NextFunction, Request, Response } from 'express'

export function requestLogger(
    request: Request, _response: Response, next: NextFunction
): void {
    // avoid logging passwords
    let password
    if (request.body.password) {
        password = request.body.password
        request.body.password = 'xxx'
    }

    console.log('Method:', request.method)
    console.log('URL:  ', request.url)
    console.log('Body:  ', request.body)
    console.log('---')

    if (request.body.password) {
        request.body.password = password
    }

    next()
}

export function unknownEndpoint(_request : Request, response: Response): void {
    response.status(404).send({ error: 'unknown endpoint' })
}

export function errorHandler(
    error: any, _request: Request, response: Response, next: NextFunction
): void {
    console.error(error.name, error.message)

    response.status(400).send({ error: `error.name: ${error.message}` })

    next(error)
}
