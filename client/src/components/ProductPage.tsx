import queryString from 'query-string'
import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'

import Product from './Product'
import Spinner from 'react-bootstrap/Spinner'
import { setNewErrorMessage } from '../reducers/notificationReducer'
import productService from '../services/products'
import { Product as ProductType } from '../types/product'
import { handleAxiosError } from '../utils'

function ProductPage() {
    const [product, setProduct] = useState<ProductType|null>(null)
    const [productLoaded, setProductLoaded] = useState(false)
    const [showSpinner, setShowSpinner] = useState(false)
    const dispatch = useDispatch()

    useEffect(() => {
        const httpParams: any = queryString.parse(window.location.search)
        if (!httpParams.productId) {
            dispatch(setNewErrorMessage(
                'no product ID provided on http parameters'
            ))
            return
        }

        setShowSpinner(true)

        productService.getOne(httpParams.productId)
            .then(p => {
                setProduct(p)
                setShowSpinner(false)
                setProductLoaded(true)
            }).catch(err => {
                handleAxiosError(dispatch, err, 'productService.getOne()')
            })
    }, [dispatch])

    return (
        <>
            <div style={{ display: showSpinner ? '' : 'none',
                position: 'fixed', top: '50%', left: '50%',
                zIndex: 100 }}>
                <Spinner animation="border" />
            </div>

            {productLoaded
                ? <div className="article-item">
                    <Product
                        product={product!}
                        limitDescription={false}
                        showLinkToProductPage={false}
                        zoomImgOnHover={false} />
                    <br />
                </div>
                : null}
            <br />
        </>
    )
}

export default ProductPage
