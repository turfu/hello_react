import React, { useEffect, useState } from 'react'
import Col from 'react-bootstrap/Col'
import Container from 'react-bootstrap/Container'
import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'
import Row from 'react-bootstrap/Row'
import Toast from 'react-bootstrap/Toast'
import { Helmet } from 'react-helmet'
import { useDispatch, useSelector } from 'react-redux'
import { BrowserRouter, Link, Route, Switch } from 'react-router-dom'

import './css/App.css'
import { ProductsCookie } from './types/cookies'
import Account from './components/Account'
import AccountValidator from './components/AccountValidator'
import Articles from './components/Articles'
import Auth from './components/Auth'
import Basket from './components/Basket'
import ChangeForgottenPasswordPage from './components/ChangeForgottenPasswordPage'
import ForgottenPasswordPage from './components/ForgottenPasswordPage'
import Logout from './components/Logout'
import ProductPage from './components/ProductPage'
import { setUserCookie } from './reducers/loginReducer'
import { RootState } from './reducers/main'
import { popMessage } from './reducers/notificationReducer'

const AlertToast: React.FC<{ msgType: string; msg: string }> =
    ({ msgType, msg }) => {
        const [show, setShow] = useState(true)
        const dispatch = useDispatch()

        useEffect(() => {
            setTimeout(() => {
                setShow(false)
            }, 5000/* ms */)
            setTimeout(() => {
                dispatch(popMessage())
            }, 6000/* ms */)
        }, [dispatch])

        return (
            <Toast
                show={show}
                id={show
                    ? (msgType === 'SUCCESS' ? 'successToast' : 'errorToast')
                    : ''}
                onClick={() => setShow(false)}
                className={msgType === 'SUCCESS' ? 'bg-success' : 'bg-danger'}
                style={{ maxWidth: window.screen.width }}>

                <Toast.Header>
                    <strong className="mr-auto">
                        {msgType === 'SUCCESS' ? <>Success</> : <>Error</>}
                    </strong>
                </Toast.Header>

                <Toast.Body>{msg}</Toast.Body>
            </Toast>
        )
    }

function Alerts() {
    const messages =
        useSelector((state: RootState) => state.notifications.messages)

    return (
        <div style={{ position: 'fixed', top: '60px', zIndex: 100 }}>
            {messages.map((msg: typeof messages[0]) =>
                <AlertToast key={msg.key}
                    msgType={msg.messageType} msg={msg.data} />)}
        </div>
    )
}

function NavigationBar() {
    const [nbBasketItems, setNbBasketItems] = useState(0)
    const userCookie = useSelector((state: RootState) => state.login.userCookie)
    const addToBasketTrigger =
          useSelector((state: RootState) => state.articles.addToBasketTrigger)

    useEffect(() => {
        let productListStr = window.localStorage.getItem('helloReactProducts')

        if (!productListStr) {
            setNbBasketItems(0)
            return
        }

        const productList: ProductsCookie = JSON.parse(productListStr)

        let totalItems = 0
        for (let idx = 0; idx < productList!.length; ++idx)
            totalItems += productList![idx].quantity

        setNbBasketItems(totalItems)
    }, [addToBasketTrigger, setNbBasketItems])

    return (
        <Navbar collapseOnSelect fixed="top" expand="sm" bg="dark" variant="dark">
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link href="#" as="span">
                        <Link to="/">
                            <i className="fas fa-home fa-lg" />
                        </Link>
                    </Nav.Link>

                    <Nav.Link href="#" as="span">
                        <Link to="/basket" id="basketLink">
                            <i className="fas fa-shopping-cart fa-lg" />
                            <sup style={{ fontSize: 'medium' }}>
                                {nbBasketItems}
                            </sup>
                        </Link>
                    </Nav.Link>

                    <Nav.Link href="#" as="span">
                        {userCookie
                            ? <>
                                logged as&nbsp;
                                <Link to="/account">
                                    {userCookie.username}
                                    &nbsp;
                                    <i className="fas fa-user-circle" />
                                </Link>
                                &nbsp; | &nbsp;
                                <Link to="/logout">
                                    Logout&nbsp;
                                    <i className="fas fa-sign-out-alt" />
                                </Link>
                            </>
                            : <Link to="/auth">
                                Login or create account&nbsp;
                                <i className="fas fa-sign-in-alt" />
                            </Link>}
                    </Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}

function CookieUsageAlert() {
    const [show, setShow] = useState(false)

    useEffect(() => {
        const acceptCookies
            = window.localStorage.getItem('helloReactAcceptCookies')
        setShow(!acceptCookies)
    }, [setShow])

    const handleClose = () => {
        setShow(false)
        window.localStorage.setItem('helloReactAcceptCookies',
            JSON.stringify({ accept: true }))
    }

    return (
        <Toast show={show} onClose={handleClose}
            style={{
                maxWidth: '100vw', position: 'fixed', bottom: 0, zIndex: 100
            }}>
            <Toast.Header>
            </Toast.Header>
            <Toast.Body>
                <p style={{ width: '95vw' }}>
                    This website uses cookies.
                    Currently, it uses only necessary cookies, to remember your
                    connection and the articles you added to your basket.
                    By visiting this website, you consent to the use of all the
                    cookies.
                </p>
            </Toast.Body>
        </Toast>
    )
}

function Footer() {
    const userCookie = useSelector((state: RootState) => state.login.userCookie)

    return (
        <Row className="footer">
            <Col sm={4} className="footer-column">
                <h5>Links</h5>
                <Link to="/">
                    Home / Articles
                </Link>
                <br />
                <Link to="/basket">Basket</Link>
                <br />
                <Link to={userCookie ? '/account' : '/auth'}>Account</Link>
            </Col>

            <Col sm={4} className="footer-column">
                Copyright (c) 2020 Florent Bordignon.
                <br />
                <br />
                The source code is available on
                &nbsp;
                <a href="https://bitbucket.org/turfu/hello_react/src/master">
                    bitbucket
                </a>
                &nbsp;
                under the MIT license.
            </Col>

            <Col sm={4} className="footer-column">
                <h5>About</h5>
                This is a Hello world website created with the MERN stack.
            </Col>
        </Row>
    )
}

function App() {
    const loggedInTrigger =
        useSelector((state: RootState) => state.login.loggedInTrigger)
    const dispatch = useDispatch()

    useEffect(() => {
        const cookie = window.localStorage.getItem('helloReactUser')
        if (cookie) {
            const parsedCookie = JSON.parse(cookie)
            dispatch(setUserCookie(parsedCookie))
        }
    }, [loggedInTrigger, dispatch])

    return (
        // add space to the top so that page content is placed after the navbar
        <div style={{ paddingTop: '60px' }}>
            {/* Bootstrap */}
            <link
                rel="stylesheet"
                href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
                integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
                crossOrigin="anonymous"
            />

            {/* Font Awesome */}
            <link
                rel="stylesheet"
                href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
                integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
                crossOrigin="anonymous"
            />

            <Helmet>
                <title>Hello React</title>
            </Helmet>

            <Container fluid>
                <CookieUsageAlert />

                <Alerts />

                <BrowserRouter>
                    <div>
                        <NavigationBar />
                    </div>

                    <Switch>
                        <Route path="/account">
                            <Account />
                        </Route>

                        <Route path="/auth">
                            <Auth />
                        </Route>

                        <Route path="/basket">
                            <Basket />
                        </Route>

                        <Route path="/change-forgotten-password">
                            <ChangeForgottenPasswordPage />
                        </Route>

                        <Route path="/forgotten-password">
                            <ForgottenPasswordPage />
                        </Route>

                        <Route path="/logout">
                            <Logout />
                        </Route>

                        <Route path="/product">
                            <ProductPage />
                        </Route>

                        <Route path="/validate-account">
                            <AccountValidator />
                        </Route>

                        <Route path="/">
                            <Articles />
                        </Route>
                    </Switch>

                    <Footer />
                </BrowserRouter>
            </Container>
        </div>
    )
}

export default App
